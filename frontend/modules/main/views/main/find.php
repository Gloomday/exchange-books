<?
use yii\helpers\Html;
?>

<div class="properties-listing spacer">

    <div class="row">
        <div class="col-lg-3 col-sm-4 ">
            <?=\yii\helpers\Html::beginForm(\yii\helpers\Url::to('/main/main/find/'),'get') ?>
            <div class="search-form"><h4><span class="glyphicon glyphicon-search"></span>Пошук</h4>
                <?=Html::textInput('propert', $request->get('propert'), ['class' => 'form-control', 'placeholder' => 'Шукати по назві, автору, опису']) ?>
                <div class="row">
                    <div class="col-lg-12">
                        <?=Html::dropDownList('year', $request->get('year'),[
                            '1970-1975' => '1970 - 1975',
                            '1975-1980' => '1975 - 1980',
                            '1980-1985' => '1980 - 1985',
                            '1985-1990' => '1985 - 1990',
                            '1990-1995' => '1990 - 1995',
                            '2000-2005' => '2000 - 2005',
                            '2005-2010' => '2005 - 2010',
                            '2010-2015' => '2010 - 2015',
                            '2015' =>'2015 - ...',
                        ],['class' => 'form-control', 'prompt' => 'Рік видання']) ?>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-12">
                        <?=Html::dropDownList('genre', $request->get('genre'),[
                            'Біографія',
                            'Бойовики',
                            'Вестерн',
                            'Детектив',
                            'Драма',
                            'Класика',
                            'Комп`ютерна література',
                            'Легенди і міфи',
                            'Містика',
                            'Повість',
                            'Поема',
                            'Роман',
                            'Казка',
                            'Фантастика',
                            'Фентезі',
                            'Інше',
                        ],['class' => 'form-control', 'prompt' => 'Жанр']) ?>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-12">
                        <?=Html::dropDownList('in_stock', '',[
                            'Не в наявності',
                            'В наявності',
                        ],['class' => 'form-control', 'prompt' => 'Наявність:']) ?>
                    </div>
                </div>

                <button class="btn btn-primary">Шукати</button>
                <?=\yii\helpers\Html::endForm() ?>

            </div>



            <div class="hot-properties hidden-xs">

                <? echo \frontend\widgets\HotWidget::widget() ?>

            </div>


        </div>

        <div class="col-lg-9 col-sm-8">
            <div class="row">

                <?
                foreach($model as $row):
                    $url = \frontend\components\Common::getUrlAdvert($row);
                    ?>
                    <!-- properties -->
                    <div class="col-lg-4 col-sm-6">
                        <div class="properties">
                            <div class="image-holder"><img src="<?=\frontend\components\Common::getImageAdvert($row)[0] ?>"  class="img-responsive" alt="properties">
                                <div class="status <?=($row['in_stock']) ? 'sold' : 'new' ?>"><?=\frontend\components\Common::getType($row) ?></div>
                            </div>
                            <h4><a href="<?=$url ?>" ><?=\frontend\components\Common::getTitleAdvert($row) ?></a></h4>
                            <p class="price">Автор: <?=$row['author'] ?></p>
                            <a class="btn btn-primary" href="<?=$url ?>" >Детальніше</a>
                        </div>
                    </div>

                    <?
                endforeach;
                ?>
                <!-- properties -->


                <div class="clearfix"></div>
                <!-- properties -->
                <div class="center">
                    <? echo \yii\widgets\LinkPager::widget([
                        'pagination' => $pages
                    ]) ?>
                </div>

            </div>
        </div>
    </div>
</div>