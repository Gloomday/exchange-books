<?php

use yii\db\Migration;


class m170512_162522_create_user_profile_table extends Migration
{
    public function safeUp()
    {
        $this->createTable('user_profile', [
            'user_id' =>'pk',
            'first_name' => $this->string(50)->notNull(),
	        'last_name' => $this->string(50)->notNull(),
	        'date_birthday' => $this->integer(11),
	        'skype' => $this->string(255)
        ]);
        $this->addForeignKey('user_profile_user_id', 'user_profile', 'user_id', 'user', 'id');
    }

    public function safeDown()
    {
        $this->dropForeignKey('user_profile_user_id', 'user_profile');
        $this->dropTable('user_profile');
    }
}
