<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\db\Query;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Adverts';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="advert-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Advert', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'user.email',
            'name_book',
            'author',
            //'genre',
            'edition',

            // 'year_book',
            // 'town_book',
            // 'general_image',
            // 'description:ntext',
            // 'location',
            // 'address',
            // 'hot',
            // 'recommend',
            // 'in_stock',
            // 'created_at',
            // 'updated_at',
            // 'category_id',


            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
