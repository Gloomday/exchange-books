<?php


namespace app\modules\main\controllers;

use common\models\User;
use common\models\Wishlist;
use frontend\models\SignupForm;
use Yii;
use common\models\LoginForm;
use common\models\UserPhone;
use yii\web\Controller;
use frontend\filters\FilterAdvert;
use common\models\UserProfile;
use yii\web\NotFoundHttpException;
use common\models\Advert;
use yii\base\DynamicModel;
use dosamigos\google\maps\LatLng;
use dosamigos\google\maps\Map;
use dosamigos\google\maps\overlays\Marker;
use frontend\components\Common;
use yii\data\Pagination;
use yii\web\UploadedFile;
use Imagine\Image\Box;
use Imagine\Image\Point;
use yii\imagine\Image;
use yii\filters\VerbFilter;
use frontend\components\Cart;
use yii\helpers\Url;



class MainController extends Controller
{
    public $layout = "inner";

    public function actionIndex()
    {
        return $this->render('index');
    }

    public function behaviors(){
        return [
            [
                'only' => ['view-advert'],
                'class' => FilterAdvert::className(),
            ]
        ];
       /* return array_merge(parent::behaviors(), [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'add-in-cart' => ['post'],
                    'delete-from-cart' => ['post'],
                ],
            ],
        ]);*/
    }

    public function actions()
    {
        return [
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
            'test' => [
                'class' => 'frontend\actions\TestAction',
            ],
            'page' => [
                'class' => 'yii\web\ViewAction',
                'layout' => 'inner',
            ]
        ];
    }

    public function actionWishlist($advert_id)
    {
        /*print_r($advert_id);
        die();*/
     $wish = new Wishlist();
     $wish->advert_id = $advert_id;
     $wish->user_id = Yii::$app->user->id;
     if ($wish->save())
     {
       return $this->redirect(Url::to('view-advert'));
     }
    }

    public function actionFind($propert='', $year='',$in_stock='', $genre = ''){
        $this->layout = 'sell';

        $query = Advert::find();
        $query->filterWhere(['like', 'name_book', $propert])
            ->orFilterWhere(['like', 'author', $propert])
            ->orFilterWhere(['like', 'description', $propert])
            ->andFilterWhere(['in_stock' => $in_stock])
            ->andFilterWhere(['genre' => $genre]);

        if($year){
            $years = explode("-",$year);

            if(isset($years[0]) && isset($years[1])) {
                $query->andWhere(['between', 'year_book', $years[0], $years[1]]);
            }
            else{
                $query->andWhere(['>=', 'year_book', $years[0]]);
            }
        }

        $countQuery = clone $query;
        $pages = new Pagination(['totalCount' => $countQuery->count()]);
        $pages->setPageSize(10);

        $model = $query->offset($pages->offset)->limit($pages->limit)->all();

        $request = \Yii::$app->request;
        return $this->render("find", ['model' => $model, 'pages' => $pages, 'request' => $request]);

    }

    public function uploadAvatar(){
        if(Yii::$app->request->isPost){
            $id = Yii::$app->user->id;
            $path = Yii::getAlias("@frontend/web/uploads/users");
            $file = UploadedFile::getInstanceByName('avatar');
            if($file) {
                $name = $id . '.jpg';
                $file->saveAs($path . DIRECTORY_SEPARATOR . $name);


                $image = $path . DIRECTORY_SEPARATOR . $name;
                $new_name = $path . DIRECTORY_SEPARATOR . "small_" . $name;

                Image::frame($image, 0, '666', 0)
                    ->thumbnail(new Box(200, 200))
                    ->save($new_name, ['quality' => 100]);

                return true;
            }
        }

    }

    public function actionUser_profile()
    {
        $user = ($user = User::findOne(\Yii::$app->user->id)) ? $user : new User();
        $user->scenario = 'setting';
        $userprofile = ($userprofile = UserProfile::findOne(\Yii::$app->user->id)) ? $userprofile : new UserProfile();
        $userphone = ($userphone = UserPhone::findOne(['user_id' => \Yii::$app->user->id])) ? $userphone : new UserPhone();


        if ($user->load(Yii::$app->request->post()) && $userprofile->load(Yii::$app->request->post()) &&
            $userphone->load(Yii::$app->request->post())) {
            $isValid = $user->validate();
            $isValid = $userprofile->validate() && $isValid;
            $isValid = $userphone->validate() && $isValid;
            if ($isValid) {
                $user->save(false);
                $userprofile->save(false);
                $this->uploadAvatar();
                foreach ($userphone as $item)
                {
                    echo $item->phone;
                }
                $userphone->save(false);
                return $this->redirect(['user_profile', 'id' => Yii::$app->user->id]);
            }
        }

        return $this->render('user_profile', [
            'user' => $user,
            'userprofile' => $userprofile,
            'userphone' => $userphone,
        ]);
    }




    public function actionRegister()
    {
        $model = new SignupForm();
        if(isset($_POST['SignupForm']))
        {
            //var_dump($_POST['SignupForm']); die();
            $model->attributes = \Yii::$app->request->post('SignupForm');

            if($model->validate() &&  $model->signup())
            {
                return $this->goHome();
            }
        }
        return $this->render("register", ['model'=>$model]);
    }

    public function actionLogin()
    {
        $model = new LoginForm();
        if($model->load(\Yii::$app->request->post()) && $model->login()){

            $this->goBack();
        }

        return $this->render("login", ['model' => $model]);
    }

    public function actionViewAdvert($id){
        $model = Advert::findOne($id);

        $data = ['name', 'email', 'text'];
        $model_feedback = new DynamicModel($data);
        $model_feedback->addRule('name','required');
        $model_feedback->addRule('email','required');
        $model_feedback->addRule('text','required');
        $model_feedback->addRule('email','email');


        if(\Yii::$app->request->isPost) {
            if ($model_feedback->load(\Yii::$app->request->post()) && $model_feedback->validate()){

                \Yii::$app->common->sendMail('Subject Advert',$model_feedback->text);
            }

        }

        $user = $model->user;
        $images = \frontend\components\Common::getImageAdvert($model,false);

        $current_user = ['email' => '', 'username' => ''];

        if(!\Yii::$app->user->isGuest){

            $current_user['email'] = \Yii::$app->user->identity->email;
            $current_user['username'] = \Yii::$app->user->identity->username;

        }

        $coords = str_replace(['(',')'],'',$model->location);
        $coords = explode(',',$coords);

        $coord = new LatLng(['lat' => $coords[0], 'lng' => $coords[1]]);
        $map = new Map([
            'center' => $coord,
            'zoom' => 20,
        ]);

        $marker = new Marker([
            'position' => $coord,
            'title' => Common::getTitleAdvert($model),
        ]);

        $map->addOverlay($marker);


        return $this->render('view_advert',[
            'model' => $model,
            'model_feedback' => $model_feedback,
            'user' => $user,
            'images' =>$images,
            'current_user' => $current_user,
            'map' => $map
        ]);

    }

}