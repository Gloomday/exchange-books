<?php

use yii\db\Migration;


class m170512_164543_create_category_table extends Migration
{

    public function safeUp()
    {
        $this->createTable('category', [
            'id' => 'pk',
            'parent_id'=>$this->integer(11)->notNull(),
	        'name'=>$this->text(255)->notNull()
        ]);

        $this->addForeignKey('category_category', 'category', 'parent_id', 'category', 'id');
    }


    public function safeDown()
    {
        $this->dropForeignKey('category_category', 'category');
        $this->dropTable('category');
    }
}
