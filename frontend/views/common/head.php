<?
use yii\bootstrap\Nav;
?>
<!-- Header Starts -->
<div class="navbar-wrapper">

    <div class="navbar-inverse" role="navigation">
        <div class="container">
            <div class="navbar-header">


                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

            </div>



            <div class="navbar-collapse  collapse">
                <?
                $menuItems = [];
                $menuItems[] = ['label' => 'Головна', 'url'=> ['/']];
                $menuItems[] = ['label' => 'Про нас', 'url'=> ['/main/main/page\', \'view\' => \'about\'']];
                $menuItems[] = ['label' => 'Контакти', 'url'=> ['/main/main/contact']];

                echo Nav::widget([
                    'options' => ['class' => 'navbar-nav navbar-right'],
                    'items' => $menuItems,
                ]);
                ?>
            </div>
            <!-- #Nav Ends -->

        </div>
    </div>

</div>
<!-- #Header Starts -->





<div class="container">

    <!-- Header Starts -->
    <div class="header">
        <a href="/" ><img src="/images/logo.png"  alt="Realestate"></a>


            <?php
            $menuItems = [];
            $guest = Yii::$app->user->isGuest;
            if($guest)
            {
                $menuItems[] = ['label' => 'Увійти', 'url'=> ['/main/main/login']];
                $menuItems[] = ['label' => 'Зареєструватися', 'url'=> ['/main/main/register']];

            }
            else
            {
                $menuItems[] = ['label' => 'Оголошення', 'url'=> ['/cabinet/advert']];
                $menuItems[] = ['label' => 'Профіль', 'url'=> ['/main/main/user_profile']];
                $menuItems[] = ['label' => 'Змінити пароль', 'url'=> ['/cabinet/default/change-password']];
                $menuItems[] = ['label' => 'Кошик', 'url'=> ['/main/main/wishlist']];
                $menuItems[] = ['label' => 'Вийти', 'url' => ['/site/logout'], 'linkOptions' => ['data-method' => 'post']];
            }

            echo Nav::widget([
                'options' => ['class' => 'pull-right'],
                'items' => $menuItems,
            ]);
            ?>


    </div>
    <!-- #Header Starts -->
</div>