<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use unclead\multipleinput\MultipleInput;

/* @var $this yii\web\View */
/* @var $userprofile common\models\UserProfile */
/* @var $user common\models\User */
/* @var $userphone common\models\UserPhone */
/* @var $form ActiveForm */
?>
<div class="main-user_profile">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data'],]); ?>

    <?=\yii\helpers\Html::img(\common\components\UserComponent::getUserImage(Yii::$app->user->id), ['width' => 200]) ?>

        <?= $form->field($user, 'username')?>
        <?= $form->field($user, 'email')?>
        <?= $form->field($userprofile, 'first_name') ?>
        <?= $form->field($userprofile, 'last_name') ?>
        <?= $form->field($userprofile, 'skype') ?>
        <?= $form->field($userphone, 'phone') ?>
        <?= $form->field($userphone, 'phone') ?>

    <?=\yii\helpers\Html::label('Avatar') ?>
    <?=\yii\helpers\Html::fileInput('avatar') ?>


        <div class="form-group">
            <?= Html::submitButton('Редагувати', ['class' => 'btn btn-primary']) ?>
        </div>
    <?php ActiveForm::end(); ?>
</div><!-- main-user_profile -->
