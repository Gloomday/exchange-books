<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\UserPhone */
/* @var $form ActiveForm */
?>
<div class="main-main-phone">

    <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'phone') ?>

        <div class="form-group">
            <?= Html::submitButton('Додати телефон', ['class' => 'btn btn-primary']) ?>
        </div>
    <?php ActiveForm::end(); ?>

</div><!-- main-main-phone -->
