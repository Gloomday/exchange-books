<?php

namespace app\modules\cabinet\controllers;

use frontend\models\ChangePasswordForm;
use yii\web\Controller;
use common\models\User;
use Yii;
use common\models\UserPhone;
use common\models\UserProfile;
use yii\web\UploadedFile;
use Imagine\Image\Box;
use Imagine\Image\Point;
use yii\imagine\Image;



/**
 * Default controller for the `cabinet` module
 */
class DefaultController extends Controller
{

    public $layout = "inner";
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function init(){
        Yii::$app->view->registerJsFile('http://maps.googleapis.com/maps/api/js?key=AIzaSyAnqvbLDf6s38UpwbsBIqi0Yi4EF2KkkM8',['position' => \yii\web\View::POS_HEAD]);
    }


    public function actionChangePassword(){

        $model = new ChangePasswordForm();

        if($model->load(\Yii::$app->request->post()) && $model->changepassword()){

            $this->goHome();

        }

        return $this->render('change-password',['model' => $model]);
    }



}
