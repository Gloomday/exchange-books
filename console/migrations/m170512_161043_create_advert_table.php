<?php

use yii\db\Migration;


class m170512_161043_create_advert_table extends Migration
{

    public function safeUp()
    {
        $this->createTable('advert', [
            'id' => 'pk',
            'name_book' => $this->string(40)-> notNull(),
	        'author'=> $this->string(40)-> notNull(),
	        'genre' => $this-> string(40),
	        'edition' =>$this-> string(40),
	        'year_book' =>$this-> integer(4),
	        'town_book' =>$this-> string(40),
	        'general_image' =>$this-> string(255)-> notNull(),
	        'description' =>$this-> text() -> notNull(),
	        'location' => $this->string(100)-> notNull(),
	        'address' => $this->string(255)-> notNull(),
	        'hot' => $this->integer(1)-> notNull(),
	        'recommend' =>$this-> integer(1) -> notNull(),
	        'in_stock' => $this->integer(1)-> notNull(),
	        'created_at' => $this->integer(11) -> notNull(),
	        'updated_at' => $this->integer(11)-> notNull(),
	        'category_id' => $this->integer(11)->notNull(),
	        'user_id' =>$this->integer(11) -> notNull(),
        ]);

        $this->addForeignKey('advert_user', 'advert', 'user_id', 'user', 'id');
        //$this->addForeignKey('advert_category', 'advert', 'category_id', 'category', 'id');
    }

    public function safeDown()
    {
        $this->dropForeignKey('advert_user', 'advert');
        $this->dropTable('advert');
    }
}
