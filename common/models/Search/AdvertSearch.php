<?php

namespace common\models\Search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Advert;

/**
 * AdvertSearch represents the model behind the search form about `common\models\Advert`.
 */
class AdvertSearch extends Advert
{
    public function rules()
    {
        return [
            [['id', 'year_book', 'user_id', 'hot', 'in_stock', 'category_id', 'recommend', 'created_at', 'updated_at'], 'integer'],
            [['address','name_book', 'author', 'genre', 'edition', 'general_image', 'description', 'location', 'town_book'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Advert::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'year_book' => $this->year_book,
            'user_id' => $this->user_id,
            'hot' => $this->hot,
            'in_stock' => $this->in_stock,
            'recommend' => $this->recommend,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'address', $this->address])
            ->andFilterWhere(['like', 'name_book', $this->name_book])
            ->andFilterWhere(['like', 'author', $this->author])
            ->andFilterWhere(['like', 'genre', $this->genre])
            ->andFilterWhere(['like', 'edition', $this->edition])
            ->andFilterWhere(['like', 'town_book', $this->town_book])
            ->andFilterWhere(['like', 'general_image', $this->general_image])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'location', $this->location]);

        return $dataProvider;
    }
}
