<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Advert */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="advert-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name_book')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'author')->textInput(['maxlength' => true]) ?>

    <!--?= $form->field($model, 'genre')->textInput(['maxlength' => true]) ?-->

    <?= $form->field($model, 'genre')->dropDownList(['Біографія','Бойовики', 'Вестерн', 'Детектив',
        'Драма', 'Класика', 'Комп`ютерна література','Легенди і міфи', 'Містика', 'Повість','Поема', 'Роман',
        'Казка','Фантастика', 'Фентезі', 'Інше']) ?>

    <?= $form->field($model, 'edition')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'year_book')->textInput() ?>

    <?= $form->field($model, 'town_book')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

    <!--?= $form->field($model, 'location')->textInput(['maxlength' => true]) ?-->

    <?= $form->field($model, 'address')->textInput(['maxlength' => true]) ?>

    <div id="map_canvas" style="width:640px; height:380px"></div><br/>

    <?= $form->field($model, 'location')->hiddenInput()->label(false) ?>

    <?= $form->field($model, 'hot')->radioList(['Ні', 'Так']) ?>

    <?= $form->field($model, 'recommend')->radioList(['Ні', 'Так']) ?>

    <?= $form->field($model, 'in_stock')->radioList(['Ні', 'Так'])->label('В наявності?') ?>

    <?
    $this->registerJs("
       var geocoder;
        var map;
        var marker;
         var markers = [];

        function initialize(){

              var latlng = new google.maps.LatLng(49.414871,26.9935255);

            var options = {
                zoom: 10,
                center: latlng,
            };
            map = new google.maps.Map(document.getElementById('map_canvas'), options);
            geocoder = new google.maps.Geocoder();

        }

          function DeleteMarkers() {
        for (var i = 0; i < markers.length; i++) {
            markers[i].setMap(null);
                }
                markers = [];
            }

        function findLocation(val){

            geocoder.geocode( {'address': val}, function(results, status) {

            var location = results[0].geometry.location
            map.setCenter(location)
            map.setZoom(15)
            DeleteMarkers()

            $('#advert-location').val(location)

             marker = new google.maps.Marker({
                map: map,
                draggable: true,
                position: location
            });

          google.maps.event.addListener(marker, 'dragend', function()
        {
                    $('#advert-location').val(marker.getPosition())
        });

        markers.push(marker);

        })
        }

        $(document).ready(function() {

            initialize();

            if( $('#advert-address').val()){
             _location = $('#advert-address').val()
               findLocation(_location)
               }

            $('#advert-address').bind('blur keyup',function(){
               _location = $('#advert-address').val()
               findLocation(_location)
            })


        });"

    );
    ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
