<?php
use yii\widgets\ActiveForm;
use yii\bootstrap\Html;
?>
<div class="row register">
    <div class="col-lg-6 col-lg-offset-3 col-sm-6 col-sm-offset-3 col-xs-12 ">

        <?php $form = ActiveForm::begin() ?>

        <?= $form->field($user, 'username') ?>
        <?= $form->field($user, 'email') ?>
        <?= $form->field($user, 'password')->passwordInput() ?>
        <?= $form->field($user, 'repassword')->passwordInput() ?>



        <?= Html::submitButton('Зареєструватися', ['class' => 'btn btn-success']) ?>

        <?php $form = ActiveForm::end() ?>

    </div>
</div>