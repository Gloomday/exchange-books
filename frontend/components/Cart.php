<?php

namespace frontend\components;

use common\models\User;
use Yii;
use common\models\Wishlist;
use yii\base\Component;

/*
 * Class Cart
 * @package frontend\components
 * @property User $user
 * @property string $status
 */

class Cart extends Component
{
    const SESSION_KEY = 'user_id';
    private $_user;

    public function add($advertId)
    {
        $link = Wishlist::findOne(['advert_id' => $advertId, 'user_id' => Yii::$app->user->id]);
        if (!$link) {
            $link = new Wishlist();
        }
        $link->advert_id = $advertId;
        $link->user_id = Yii::$app->user->id;
        return $link->save();
    }

    public function createUser()
    {
        $user = new User();
        if ($user->save()) {
            $this->_user = $user;
            return true;
        }
        return false;
    }

    private function getUserId()
    {
        if (!Yii::$app->session->has(self::SESSION_KEY)) {
            if ($this->createOrder()) {
                Yii::$app->session->set(self::SESSION_KEY, $this->_user->id);
            }
        }
        return Yii::$app->session->get(self::SESSION_KEY);
    }

    public function getUser()
    {
        if ($this->_user == null) {
            $this->_user = User::findOne(['id' => $this->getUserId()]);
        }
        return $this->_user;
    }

    public function delete($advertId)
    {
        $link = Wishlist::findOne(['advert_id' => $advertId, 'user_id' => $this->getUserId()]);
        if (!$link) {
            return false;
        }
        return $link->delete();
    }

    public function isEmpty()
    {
        if (!Yii::$app->session->has(self::SESSION_KEY)) {
            return true;
        }
        return $this->user->productsCount ? false : true;
    }

    public function getStatus()
    {
        if ($this->isEmpty()) {
            return Yii::t('app', 'В Wish List пусто');
        }
        return Yii::t('app', 'В Wish List {productsCount, number} {productsCount, plural, one{товар} few{товара} many{товаров} other{товара}} ', [
            'amount' => $this->user->amount
        ]);
    }
}