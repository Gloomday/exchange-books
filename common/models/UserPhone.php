<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "user_phone".
 *
 * @property integer $id
 * @property string $phone
 * @property integer $user_id
 *
 * @property User $user
 * @property UserProfile $userprofile
 */
class UserPhone extends \yii\db\ActiveRecord
{

    public static function tableName()
    {
        return 'user_phone';
    }


    public function rules()
    {
        return [
            [['phone', 'user_id'], 'required'],
            [['user_id'], 'integer'],
            [['phone'], 'string', 'max' => 13],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }


    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'phone' => 'Телефон',
            'user_id' => 'User ID',
        ];
    }

    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /*public function updateUserPhone()
    {
        $userphone = ($userphone = UserPhone::findOne(Yii::$app->user->id)) ? $userphone : new UserPhone();
        $userphone = ($userphone = UserPhone::findOne(['user_id' => \Yii::$app->user->id])) ? $userphone : new UserPhone();
        $userphone = ($userphone = UserPhone::find()->where(['user_id'=> Yii::$app->user->id])->all()) ? $userphone : new UserPhone();

            $userphone->user_id = $this->id;
            $userphone->phone = $this->phone;

            return $userphone->save();

        }*/



}
