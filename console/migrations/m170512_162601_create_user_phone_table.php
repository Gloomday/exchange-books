<?php

use yii\db\Migration;


class m170512_162601_create_user_phone_table extends Migration
{

    public function safeUp()
    {
        $this->createTable('user_phone', [
            'id' => 'pk',
            'phone'=>$this-> string(13)->notNull(),
	        'user_id'=>$this-> integer(11)->notNull()
        ]);

        $this->addForeignKey('user_phone_user_id','user_phone', 'user_id', 'user', 'id');
    }

    public function safeDown()
    {
        $this->dropForeignKey('user_phone_user_id', 'user_phone');
        $this->dropTable('user_phone');
    }
}
