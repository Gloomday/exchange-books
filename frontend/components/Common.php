<?
namespace frontend\components;
use Yii;
use common\models\Wishlist;
use yii\base\Component;
use yii\helpers\BaseFileHelper;
use yii\helpers\Url;

class Common extends Component{

    const EVENT_NOTIFY = 'notify_admin';

    public function sendMail($subject,$text,$emailFrom='dorozhanskij@yandex.ru',$nameFrom='Alex'){
        if(\Yii::$app->mail->compose()
            ->setFrom(['dorojanskiyy@rambler.ru' => 'Alex'])
            ->setTo([$emailFrom => $nameFrom])
            ->setSubject($subject)
            ->setHtmlBody($text)
            ->send()){
            $this->trigger(self::EVENT_NOTIFY);
            return true;
        }
    }

    public function notifyAdmin($event){

        print "Notify Admin";
    }

    public static function AddWishList($id)
    {
        $link = Wishlist::findOne(['advert_id' => $id, 'user_id' => Yii::$app->user->id]);
        if (!$link) {
            $link = new Wishlist();
        }
        $link->advert_id = $id;
        $link->user_id = Yii::$app->user->id;
        $link->save();
        //return $this->redirect(["wishlist", 'id' => Yii::$app->user->id]);
    }

    public static function getTitleAdvert($data){

        return $data['name_book'];
    }

    public static function getImageAdvert($data,$general = true,$original = false){

        $image = [];
        $base = '/';
        if($general){

            $image[] = $base.'uploads/adverts/'.$data['id'].'/general/small_'.$data['general_image'];
        }
        else{
            $path = \Yii::getAlias("@frontend/web/uploads/adverts/".$data['id']);
            $files = BaseFileHelper::findFiles($path);

            foreach($files as $file){
                if (strstr($file, "small_") && !strstr($file, "general")) {
                    $image[] = $base . 'uploads/adverts/' . $data['id'] . '/' . basename($file);
                }
            }
        }

        return $image;
    }

    public static function substr($text,$start=0,$end=80){

        return mb_substr($text,$start,$end);
    }


    public static function getType($row){
        return ($row['in_stock']) ? 'В наявності' : ' Не в наявності';
    }

    public static function getUrlAdvert($row)
    {
        return Url::to(['/main/main/view-advert', 'id' => $row['id']]);
    }

}