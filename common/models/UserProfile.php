<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "user_profile".
 *
 * @property integer $user_id
 * @property string $first_name
 * @property string $last_name
 * @property integer $date_birthday
 * @property string $skype
 *
 * @property User $user
 * @property UserPhone $userphone
 */
class UserProfile extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_profile';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['first_name', 'last_name'], 'required'],
            [['date_birthday'], 'integer'],
            [['first_name', 'last_name'], 'string', 'max' => 50],
            [['skype'], 'string', 'max' => 255],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'user_id' => 'User ID',
            'first_name' => 'Ім`я',
            'last_name' => 'Прізвище',
            'date_birthday' => 'Дата народження',
            'skype' => 'Skype',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public function updateUserProfile()
    {
        $userprofile = ($userprofile = UserProfile::findOne(Yii::$app->user->id)) ? $userprofile : new UserProfile();

        $userprofile->user_id = Yii::$app->user->id;
        $userprofile->first_name = $this->first_name;
        $userprofile->last_name = $this->last_name;
        $userprofile->date_birthday = $this->date_birthday;
        $userprofile->skype = $this->skype;

        return  $userprofile->save() ? true : false;
    }
}
