<?php

namespace common\models;

use frontend\components\Common;
use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "advert".
 *
 * @property integer $id
 * @property string $name_book
 * @property string $author
 * @property string $genre
 * @property string $edition
 * @property integer $year_book
 * @property string $town_book
 * @property string $general_image
 * @property string $description
 * @property string $location
 * @property string $address
 * @property integer $hot
 * @property integer $recommend
 * @property integer $in_stock
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $category_id
 * @property integer $user_id
 *
 * @property User $user
 * @property Comments[] $comments
 * @property Wishlist[] $wishlists
 */
class Advert extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'advert';
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    public function scenarios(){
        $scenarios = parent::scenarios();
        $scenarios['step2'] = ['general_image'];
        $scenarios['admin'] = ['name_book', 'author', 'description', 'year_book', 'hot', 'recommend', 'address',
            'location', 'edition', 'town book', 'genre', 'in_stock', 'created_at', 'updated_at'];

        return $scenarios;
    }



    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name_book', 'author',  'description'], 'required'],
            [['year_book', 'hot', 'recommend', 'in_stock', 'created_at', 'updated_at', 'category_id', 'genre', 'user_id'], 'integer'],
            [['description'], 'string'],
            [['name_book', 'author',  'edition', 'town_book'], 'string', 'max' => 40],
            [['general_image', 'address'], 'string', 'max' => 255],
            [['location'], 'string', 'max' => 100],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }


    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name_book' => 'Назва книги',
            'author' => 'Автор',
            'genre' => 'Жанр',
            'edition' => 'Видавництво',
            'year_book' => 'Рік видачі',
            'town_book' => 'Місто видання',
            'general_image' => 'General Image',
            'description' => 'Опис',
            'location' => 'Location',
            'address' => 'Address',
            'hot' => 'Гарячі',
            'recommend' => 'Рекомендовані',
            'type' => 'Тип',
            'in_stock' => 'В наявності',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'category_id' => 'Category ID',
            'user_id' => 'User ID',
        ];
    }

    public function getComments()
    {
        return $this->hasMany(Comments::className(), ['advert_id' => 'id']);
    }

    public function getTitle(){

        return Common::getTitleAdvert($this);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWishlists()
    {
        return $this->hasMany(Wishlist::className(), ['advert_id' => 'id']);
    }

    public function getUser(){
        return $this->hasOne(User::className(),['id' => 'user_id']);
    }



    /**
     * @inheritdoc
     * @return AdvertQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new AdvertQuery(get_called_class());
    }
}
