<?php

namespace common\models;

use frontend\components\Common;
use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "wishlist".
 *
 * @property integer $id
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $advert_id
 * @property integer $user_id
 *
 * @property Advert $advert
 * @property User $user
 */
class Wishlist extends \yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'wishlist';
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    public function rules()
    {
        return [
            [['advert_id', 'user_id'], 'required'],
            [['created_at', 'updated_at', 'advert_id', 'user_id'], 'integer'],
            [['advert_id'], 'exist', 'skipOnError' => true, 'targetClass' => Advert::className(), 'targetAttribute' => ['advert_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'advert_id' => 'Advert ID',
            'user_id' => 'User ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAdvert()
    {
        return $this->hasOne(Advert::className(), ['id' => 'advert_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @inheritdoc
     * @return WishlistQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new WishlistQuery(get_called_class());
    }

    public function add($idadvert)
    {
        $wishlist = new Wishlist();
        $wishlist->user_id = Yii::$app->user->id;
        $wishlist->advert_id = $idadvert;
        return  $wishlist->save() ? true : false;
    }
}
